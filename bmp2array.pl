#use strict;
use warnings;

#BMP を読み込み
#http://fireball.loafer.jp/kes/article_51.html

#BMPファイル形式
#http://www.syuhitu.org/other/bmp/bmp.html

if(@ARGV == 0)
{
	die("error\n");
}

my $path = $ARGV[0];

open FILE, '<', $path or die "file open error: $!";

use integer;

my $chunk;
binmode(FILE);

# BITMAPFILEHEADER を読み込む。
read FILE, $chunk, 14 or die $!;
my ($signature, $data_offset) = unpack('a2x8V', $chunk);

# フォーマットのチェック。
$signature eq 'BM' or die 'Invalid BITMAP.';

# BITMAPINFOHEADER を読み込む。
read FILE, $chunk, 40 or die $!;
my ($bih_size, $width, $height, $planes, $bit_count, $compression) = unpack('VVVvvVx20', $chunk);

# フォーマットのチェック。
$planes == 1 or die 'Invalid BITMAP.';
$bit_count == 24 or $bit_count == 32 or die 'Not supported bit count.';
$compression == 0 or die 'Not supported compression.';

# 残りのヘッダを無視し、データ部まで読み飛ばす。
if ($data_offset - 54) {
	read FILE, $chunk, $data_offset - 54 or die $!;
}

# データ読み取り開始。
my @data = ();

# ピクセルデータのサイズ＋詰め物サイズ＝行のサイズ。
my $part_size = $width * $bit_count / 8;
my $padding_size = (-$part_size) & 0x3;

# 解説していなかったが、高さが負の場合、
# トップダウンの BMP である。（上から下に格納）
my $is_top_down  = $height < 0;

# 高さを正の値に直しておく。
$height = -$height if $is_top_down;

foreach (1 .. $height) {
	# ピクセルデータ読み出し。
	read FILE, $chunk, $part_size or die $!;

	# 1 行を RGB の 32 ビット整数配列にデコード。
	my $line;
	if ($bit_count == 32) {
		# 32 ビットなので自然にデコードできる。
		$line = [ unpack('V*', $chunk) ];
	} else {
		$line = [];
		# 3 バイトずつ切り出し、最後に 0 のバイトを
		# つけて 32 ビット長とし、デコード。
		push @$line, unpack('V', $& . "\0")
		  while $chunk =~ /.{3}/sg;
	}
	if ($is_top_down) {
		# トップダウンの場合は先頭行から。
		push @data, $line;
	} else {
		# ボトムアップ（通常）の場合は末尾行から。
		unshift @data, $line;
	}

	# 詰め物を読み捨てる。
	if ($padding_size) {
		read FILE, $chunk, $padding_size or die $!;
	}
}

$t = \@data;
$width = scalar(@{$t->[0]});
$height = scalar(@$t);

print 'width:' . $width . "\n";
print "height:" . $height . "\n";

#	 return \@data;

close FILE;

#print "{\n";

for($y = 0; $y < $height; $y++)
{
	for($x = 0; $x < $width; $x++)
	{
		$pixel = $t->[$y][$x];
		my ($red, $green, $blue) = unpack('xCCC', pack('N', $pixel)); 
		
		if($x == 0 && $y == 0)
		{
			print " ";
		}
		else
		{
			print ",";
		}
		
		printf("0x%02X%02X%02X" , $red, $green, $blue);
	}
	
	print "\n";
}

#print "}\n";
