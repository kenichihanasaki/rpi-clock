CFLAGS=-Wall -O3 -g
CXXFLAGS=-Wall -O3 -g -std=c++0x -I /usr/local/include
OBJECTS=main.o gpio.o led-matrix.o thread.o
BINARIES=clock
LDFLAGS=-lrt -lm -lpthread

all : $(BINARIES)

led-matrix.o: led-matrix.cc led-matrix.h
main.o: led-matrix.h

clock : $(OBJECTS)
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LDFLAGS)

clean:
	rm -f $(OBJECTS) $(BINARIES)
