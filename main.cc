#include "thread.h"
#include "led-matrix.h"

#include <assert.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <algorithm>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <signal.h>
#include <time.h>

#include <string.h>

// Base-class for a Thread that does something with a matrix.

volatile bool g_running;

class RGBMatrixManipulator : public Thread {
public:

    //RGBMatrixManipulator(RGBMatrix *m) : running_(true), matrix_(m)

    RGBMatrixManipulator(RGBMatrix *m) : matrix_(m) {
    }

    virtual ~RGBMatrixManipulator() {
        //running_ = false;
    }

    // Run() implementation needs to check running_ regularly.

protected:
    //volatile bool running_; // TODO: use mutex, but this is good enough for now.

    RGBMatrix * const matrix_;
};

/**
 * Pump pixels to screen. Needs to be high priority real-time because jitter
 * here will make the PWM uneven.
 */
class DisplayUpdater : public RGBMatrixManipulator {
public:

    /**
     * 
     */
    DisplayUpdater(RGBMatrix *m) : RGBMatrixManipulator(m) {
    }

    /**
     * 
     */
    void Run() {
        while (g_running) {
            matrix_->UpdateScreen();
        }
    }
};

/**
 * 
 */
class ClockThread : public RGBMatrixManipulator {
private:
    /**
     * 
     */
    void SetPixcel(int x, int y, int r, int g, int b) {
        const int width = getRGBMatrix()->width();
        const int height = getRGBMatrix()->height();

        if (x < 0 || x >= width || y < 0 || y >= height) {
            return;
        }

        getRGBMatrix()->SetPixel(x, y, r, g, b);
    }

    /**
     * 
     */
    static void signalHandler(int sig)
    {
        g_running = false;
    }
    
    /**
     * 
     */
    void setNumber(int* buffer, int n, int offsetX, int offsetY, int r, int g, int b)
    {
        static const int tbl[10][5][5] = 
        {
            //0
            {
                {1, 1, 1, 1, 1,},
                {1, 0, 0, 0, 1,},
                {1, 0, 0, 0, 1,},
                {1, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
            },
            //1
            {
                {0, 0, 0, 0, 1,},
                {0, 0, 0, 0, 1,},
                {0, 0, 0, 0, 1,},
                {0, 0, 0, 0, 1,},
                {0, 0, 0, 0, 1,},
            },
            //2
            {
                {1, 1, 1, 1, 1,},
                {0, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
                {1, 0, 0, 0, 0,},
                {1, 1, 1, 1, 1,},
            },
            //3
            {
                {1, 1, 1, 1, 1,},
                {0, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
                {0, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
            },
            //4
            {
                {1, 0, 0, 0, 1,},
                {1, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
                {0, 0, 0, 0, 1,},
                {0, 0, 0, 0, 1,},
            },
            //5
            {
                {1, 1, 1, 1, 1,},
                {1, 0, 0, 0, 0,},
                {1, 1, 1, 1, 1,},
                {0, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
            },
            //6
            {
                {1, 1, 1, 1, 1,},
                {1, 0, 0, 0, 0,},
                {1, 1, 1, 1, 1,},
                {1, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
            },
            //7
            {
                {1, 1, 1, 1, 1,},
                {0, 0, 0, 0, 1,},
                {0, 0, 0, 0, 1,},
                {0, 0, 0, 0, 1,},
                {0, 0, 0, 0, 1,},
            },
            //8
            {
                {1, 1, 1, 1, 1,},
                {1, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
                {1, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
            },
            //9
            {
                {1, 1, 1, 1, 1,},
                {1, 0, 0, 0, 1,},
                {1, 1, 1, 1, 1,},
                {0, 0, 0, 0, 1,},
                {0, 0, 0, 0, 1,},
            },
        };
        
        int x;
        int y;
        
        for(y = 0; y < 5; y++)
        {
            for(x = 0; x < 5; x++)
            {
                if(tbl[n][y][x] == 1)
                {
                    buffer[(32 * (offsetY + y)) + (offsetX + x)] = (r << 16) | (g << 8) | (b << 0);
                }
            }
        }
    }
    
    /**
     * 
     */
    void onMinuteChange(int year, int month, int day, int hour, int minute, int second)
    {
        static int buffer[16 * 32];
        memset(&buffer, 0, sizeof(buffer));
        
//        int buffer[] = 
//        {
//   0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x002424,0x000000,0x002424,0x000000,0x000000,0x000000,0x002424,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x002424,0x000000,0x002424,0x000000,0x000000,0x000000,0x002424,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x002424,0x000000,0x000000,0x000000,0x002424,0x000000,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x002424,0x000000,0x000000,0x000000,0x000000,0x000000,0x002424,0x000000,0x000000,0x000000,0x002424,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x002424,0x000000,0x002424,0x000000,0x000000,0x000000,0x002424,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x002424,0x002424,0x002424,0x002424,0x002424,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000
//};
        int r;
        int g;
        int b;
        
        if(hour >= 0 && hour <= 11)
        {
            r = 24;
            g = 16;
            b = 0;
        }
        else
        {
            r = 0;
            g = 16;
            b = 16;
        }
        
        int x;
        int y;
        
        int n;
        
        int offsetX;
        int offsetY;
        
        
        offsetX = 3;
        offsetY = 5;
        
        n = hour / 10;
        setNumber(buffer, n, offsetX, offsetY, r, g, b);
        
        
        offsetX = 9;
        offsetY = 5;
        
        n = hour % 10;
        setNumber(buffer, n, offsetX, offsetY, r, g, b);

        
        offsetX = 18;
        offsetY = 5;
        
        n = minute / 10;
        setNumber(buffer, n, offsetX, offsetY, r, g, b);
        
        
        offsetX = 24;
        offsetY = 5;
        
        n = minute % 10;
        setNumber(buffer, n, offsetX, offsetY, r, g, b);
        
        
        for(y = 0; y < 16; y++)
        {
            for(x = 0; x < 32; x++)
            {
                r = (buffer[(32 * y) + x] >> 16) & 0xff;
                g = (buffer[(32 * y) + x] >> 8) & 0xff;
                b = (buffer[(32 * y) + x] >> 0) & 0xff;

                SetPixcel(x, y, r, g, b);
            }
        }
    }
    
protected:

    /**
     * 
     */
    RGBMatrix* getRGBMatrix() {
        return matrix_;
    }
public:

    /**
     * 
     */
    ClockThread(RGBMatrix *m) : RGBMatrixManipulator(m) {
    }

    /**
     * 
     */
    ~ClockThread() {
    }

    /**
     * 
     */
    void Run() {

        if (SIG_ERR == signal(SIGINT, ClockThread::signalHandler)) {
            exit(EXIT_FAILURE);
        }
        
        time_t past = time(NULL) - 60;
        
        while (g_running) {
            
            time_t now = time(NULL);
            
            if(past != now)
            {
                if(past / 60 != now / 60)
                {
                    struct tm* p = localtime(&now);
                    onMinuteChange(0, 0, 0, p->tm_hour, p->tm_min, p->tm_sec);
                }
                
                memcpy(&past, &now, sizeof(time_t));
            }

            usleep(1000 * 1000);
        }
    }
};

/**
 * 
 */
int main(int argc, char *argv[]) {

    GPIO io;
    if (!io.Init())
        return 1;

    RGBMatrix m(&io);

    RGBMatrixManipulator *image_gen = new ClockThread(&m);
    RGBMatrixManipulator *updater = new DisplayUpdater(&m);

    g_running = true;

    updater->Start(10); // high priority
    image_gen->Start();

    while (g_running) {
        usleep(1000);
    }

    // Stopping threads and wait for them to join.
    delete image_gen;
    delete updater;

    // Final thing before exit: clear screen and update once, so that
    // we don't have random pixels burn
    m.ClearScreen();
    m.UpdateScreen();

    return 0;
}
